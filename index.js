

function countLetter(letter, sentence) {
      let result = 0;   
    
    if(letter.length > 1){
        
        return undefined

    } else {
        for(let i = 0; i < sentence.length; i++){
            if(sentence[i] == letter){
                result++
            }
        }

        return result
    }     
}


function isIsogram(text) {
   
    text.toLowerCase()

    let letters = []
    
    for(let i = 0; i < text.length; i++){
       
        if(letters.indexOf(text[i]) !== -1){
        
            return false

        }  else {
            
            letters.push(text[i])
        }
    }

    return true
}


function purchase(age, price) {

    //Conditions:
    if(age < 13){
        // Return undefined for people aged below 13.
        return undefined
    } else if( age >= 13 && age <= 21 || age > 65 ){

        return (price * 0.8).toFixed(2)

    } else {
        
        return price.toFixed(2)
    }    

   
}

function findHotCategories(items) {
    console.log(items)
    
    let hotCategories = [];

    items.forEach( item => {
        if(item.stocks == 0){
            if(hotCategories.indexOf(item.category) == -1){
                hotCategories.push(item.category)
            }
        }
    })

    return hotCategories
}


function findFlyingVoters(candidateA, candidateB) {
    
    let flyingVoters = []

    candidateA.forEach(voterA => {
        // console.log(voter)

        if(candidateB.indexOf(voterA)  !== -1){
            flyingVoters.push(voterA)
        }
    })

    return flyingVoters
    
}



module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};